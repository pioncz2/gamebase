Wymagania: (W)WWW, (D)Devops, (S)Server, (E)Engine, (G)Grafika, (#) inne, (A) analityka, (AP) admin panel

0.7 Alpha: (do wystartowania)
+S:refaktor
#:nazwa strony, nazwy gier
E:gry w pelni testowalne automatycznie
+E:gry: mozliwosc grania na tel'
+E:ludo: dzialajaca rozgrywka, nowa gra
E:labirynt: dzialajaca rozgrywka, nowa gra
S:zapisywanie profili po stronie serwera
W:logowanie fb/gp/tw
S:profil gracza: name, poziom, liczba gier, rankingi, achievmenty
A: wejsc na strony/gier, zgloszen bledu, dlugosc sesji/gry, mozliwosc ab testow
AP: za autenktykacja na osobnym repo, log servera, analityka, statystyki serwerow
W:rankingi gier
S:rankingi gier
W:explore: wybor gry, ranking, lista grajacych online
S,W:system reklam: dla profili free, podczas czekania na gre, w trakcie gry
W:wparcie wielu przegladarek
W:wparcie nexusa 5x
W:internacjonalizacja
+D:wystawienie online
S:blokada 1 gry na koncie/przegladarce
S:boty do gry: (ukryte?, losowej trudnosci), zawyzajace statystyki w explore

0.8 Beta: (do wystartowania) otwieramy zamknieta bete
S,W:rejestracja @
S,W:logowanie @
S,W:forgetAccount @, GP?, FB?
E:hotkeye do gry
S,W:czat: textowy, standardowy jak na msgr, zawsze widoczny, tworzy sie automatyczna grupa w trakcie gry
S,W:ustawienia: hotkeye
W:onboarding: opis ui
E:samouczki do gier
W:F1: lista hotkeyow, zasady gry, ranking
W:strony bledow
W:puste stany
E:gry: wybor  akcji z zoomem
S,W:404, 500
S,W:zglaszanie bledu przez usera
PA: lista zgloszonych bledow
S,W:czat: glosowy z grupa znajomych
E:wybor pionka
E:wybor kostki
A:test na dlugosc w s reklam, zbieranie akcji w sesji
#:privacy, terms and conditions, cookie
W,E:wsparcie wielu urzadzen
W,S:czat: blokada gdy user spamuje na co raz wiecej czasu
S:skalowalnosc
#:nazwa firmy
#:zrobic liste potrzebnych grafik
G:zrobienie grafik: strona www (kazda podstrona z tej wersji)
G:audyt graficzny www, gry
S:przy rozlaczeniu gracza jego pionki wracaja na spawn

0.9 beta: otwarta na ludzi z kickstartera / reklam, start firmy
S:testy obciazeniowe
D:obfuscowanie kodu
A:test ustawien usera

=kickstarter, firma, start finansowania

1.0:
4 gry
bugfixing
#:kampania reklamowa
D:mozliwosc dokupienia serwerow 24/7
W,S:customer service
#:audyt bezpieczenstwa
fx

1.1
bugfixing
Chat:  szybki dostep do czatu i listy znajomych z funkcja zaproszenia do gry
?D:rozbicie na repozytoria (testowalne osobno) - lerna js?

2.0: 
analiza analitk
gry dla firm zewnetrznych
inne rodzaje gier
cs 2d, z innymi broniami i skillami (osobne spa)

PARKING LOT:
- nowe pionki
- internacionalizacja
- reportowanie graczy
- mozliwosc robienia a/b testow

busines model:
reklamy dla kont free
konta premium bez reklam