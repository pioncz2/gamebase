module.exports = function (options) {
  return {
    playerId: options.playerId,
    roomId: options.roomId,
  };
};