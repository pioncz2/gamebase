'use strict';
const bodyParser = require('body-parser');
const express = require('express');
const app = express();
const http = require('http').Server(app);
const https = require('https');
const fs = require('fs');
const path = require('path');
const cookieParser = require('cookie-parser');
const helmet = require('helmet');
const cors = require('cors');
const io = require('socket.io')(http);
const assert = require('assert');
const serverJwt = require('./server/jwt');
const errorHandler = require('./server/error-handler');
const playersController = require('./server/players/players.controller');
const playerService = require('./server/players/player.service');
const jwt = require('jsonwebtoken');
const mongoose = require('mongoose');
const WebsocketServer = require('./server/websocket-server');

function handleError(req, res, error) {
  console.error(error.statusCode, error.error, error.options.uri);
  res.send(error.statusCode);
}

var config = require('./server/config');
let port = process.env.PORT || config.server.port;

// Process script arguments and extend config
process.argv.forEach(function (val, index) {
  if (index > 1 && val.indexOf('=') > -1) {
    console.log(index + ': ' + val);
    const [key, value,] = val.split('=');
    if (key.toLowerCase() === 'port') {
      port = +value;
    }
  }
});

const websocketServer = new WebsocketServer(io, playerService);

mongoose.set('useCreateIndex', true);
mongoose.connect(
  process.env.MONGODB_URI || config.server.mongooseConnectionString,
  { useNewUrlParser: true, },
  error => {
    console.error('Mongoose connection fail!');
    error && console.error(error);
  },
).then(() => {
  console.log('Mongoose connected!');
}, () => {
});
mongoose.Promise = global.Promise;

/**
* Module variables
*/
app.use(helmet.hidePoweredBy());
app.use(helmet.hsts());
app.use(helmet.noSniff());
app.use(helmet.xssFilter());

app.use(bodyParser.urlencoded({ extended: true, }));
app.use(bodyParser.json({ inflate: false, }));
app.use(cookieParser());
app.use(cors());
app.use(serverJwt());

app.use('/api/players/', playersController);

app.use('/ping', function(req, res) {
  console.log((new Date()).toISOString());
  res.send(200);
});

app.use('/', express.static(path.join(__dirname, 'dist')));
app.use('/static/', express.static(path.join(__dirname, 'static')));

app.use('/api/currentPlayer', (req, res) => {
  const token = req.cookies.token,
    socketId = req.cookies.io,
    // player from websocketServer may be temporary
    tempPlayer = socketId && websocketServer.connections[socketId] && websocketServer.players[websocketServer.connections[socketId].playerId];

  if (!token) {
    if (tempPlayer) {
      res.status(200).send(tempPlayer);
    } else {
      res.status(400).send({error: 'Unauthorized',});
    }
    return;
  }

  playerService.verify({token,})
    .then(playerId => {
      playerService.getById(playerId)
        .then(player => {
          res.status(200).send(player);
        }, e => {
          if (tempPlayer) {
            res.status(200).send(tempPlayer);
          } else {
            res.status(400).send({error: 'Unauthorized',});
          }
        });
    })
    .catch(e => {
      if (tempPlayer) {
        res.status(200).send(tempPlayer);
      } else {
        res.status(400).send({error: 'Unauthorized',});
      }
    });
});

app.use('/api/logout', (req, res) => {
  delete req.user;
  res.cookie('token', '').status(200).send({});
});

app.use((err, req, res, next) => {
  if (err.name === 'UnauthorizedError') {
    res.cookie('token', '').status(401).send('invalid token...');
  } else {
    next();
  }
});

app.use(function (req, res) {
  var fileName = __dirname + '/dist/index.html';
  fs.readFile(fileName, 'utf8', function (err,data) {
    if (err) {
      console.log(err);
      res.status(404).send({error: 'index not found', url: req.url,});
    } else {
      res.send(data);
    }
  });
});

http.listen(port, '0.0.0.0', function(){
  console.log('Listening on *:' + port);
});
