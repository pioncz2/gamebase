import LoginModal from './login/';
import RegistrationModal from "./registration/";
import FullscreenModal from "./fullscreenModal/";
import NameModal from "./name/";

export {
  LoginModal,
  RegistrationModal,
  FullscreenModal,
  NameModal,
};