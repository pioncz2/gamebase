## Developing

To run project locally you will need statick web files (serve or build scripts), backend (start script) and mongodb.

### Package scripts:

`npm run build` - build web application for production
`npm run start` - starts server for production
`npm run build-dev` - build web application for development
`npm run start` - starts server for development

### Mongo scripts:

`mongod` - starts mongodb server